#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    //draw 2 points
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int w, h;
	SDL_GetRendererOutputSize(ren, &w, &h);
	int x, y, p;
	x = 0;
	y = 0;
	p = A;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A) 
	{
		if (p <= 0)
		{
			p += 2 * A - 2 * x - 1;
			y++;
		}
		else
		{
			p -= 2 * x + 1;
		}
		x++;
		if (x > w || y > h) break;
		else Draw2Points(xc, yc, x, y, ren);
	}
	x = A + 1;
	y = A * 0.5;
	p = -2 * x - 1;
	while (y < h)
	{
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * A - 4 * x - 4;
			x++;
		}
		y++;
		if (x > w || y > h) break;
		else Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	int w, h;
	SDL_GetRendererOutputSize(ren, &w, &h);
	int x, y, p;
	x = 0;
	y = 0;
	p = -A;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{
		if (p > 0)
		{
			p -= 2 * A - 2 * x - 1;
			y--;
		}
		else
		{
			p += 2 * x + 1;
		}
		x++;
		if (x < -w || y < -h) break;
		else Draw2Points(xc, yc, x, y, ren);
	}
	x = A + 1;
	y = -A * 0.5;
	p = 2 * x + 1;
	while (y < h)
	{
		if (p > 0)
		{
			p -= 4 * A;
		}
		else
		{
			p -= 4 * A - 4 * x - 4;
			x++;
		}
		y--;
		if (x < -w || y < -h) break;
		else Draw2Points(xc, yc, x, y, ren);
	}
}