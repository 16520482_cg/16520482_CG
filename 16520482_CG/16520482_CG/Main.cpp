#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"
#include "Circle.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE
	Vector2D p[4];
	p[0] = { 220, 40 };
	p[1] = { 220, 260 };
	p[2] = { 35, 200 };
	p[3] = { 120, 160 };
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	for (int i = 0; i < 4; i++)
		BresenhamDrawCircle(p[i].x, p[i].y, 10, ren);
	for (int i = 0; i < 3; i++)
		Bresenham_Line(p[i].x, p[i].y, p[i + 1].x, p[i + 1].y, ren);

	SDL_SetRenderDrawColor(ren, 255, 0, 255, 255);
	DrawCurve3(ren, p[0], p[1], p[2], p[3]);


	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event

	SDL_Rect button[4];
	for (int i = 0; i < 4; i++)
	{
		button[i] = { (int)p[i].x, (int)p[i].y, 10, 10 };
	}
	bool running = true;
	bool state = false; int buttonnum;
	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			int x, y;
			SDL_GetMouseState(&x, &y);
			if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
			{
				for (int i = 0; i < 4; i++)
				{
					if ((x - button[i].x)*(x - button[i].x - button[i].w) <= 0 && (y - button[i].y)*(y - button[i].y - button[i].h) <= 0)
					{
						state = true;
						buttonnum = i;
					}
				}
			}
			if (event.button.button == SDL_BUTTON_LEFT && event.type == SDL_MOUSEBUTTONUP) state = false;
			if (state)
			{
				p[buttonnum].x = x;
				p[buttonnum].y = y;
				button[buttonnum].x = x;
				button[buttonnum].y = y;
			}
            //If the user has Xed out the window
            if( event.type == SDL_QUIT )
            {
                //Quit the program
                running = false;
            }
        }
		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
		SDL_RenderClear(ren);

		SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
		for (int i = 0; i < 4; i++)
			BresenhamDrawCircle(p[i].x, p[i].y, 10, ren);
		for (int i = 0; i < 3; i++)
			Bresenham_Line(p[i].x, p[i].y, p[i + 1].x, p[i + 1].y, ren);

		SDL_SetRenderDrawColor(ren, 255, 0, 255, 255);
		DrawCurve3(ren, p[0], p[1], p[2], p[3]);

		SDL_RenderPresent(ren);

    }



	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
