#include "Bezier.h"
#include "FillColor.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	double xt = 0, yt = 0, t = 0;
	int i = 0;
	for (t = 0; t <= 1; t += 0.0001)
	{
		xt = (1 - t)*(1 - t)*p1.x + 2 * (1 - t)*t*p2.x + t * t*p3.x;
		yt = (1 - t)*(1 - t)*p1.y + 2 * (1 - t)*t*p2.y + t * t*p3.y;
		SDL_RenderDrawPoint(ren, (int)xt, (int)yt);
	}
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	double xt = 0, yt = 0, t = 0;
	int i = 0;
	for (t = 0; t <= 1; t += 0.0001)
	{
		xt = (1 - t)*(1 - t)*(1 - t)*p1.x + 3 * (1 - t)*(1 - t)*t*p2.x + 3 * (1 - t)*t*t*p3.x + t * t*t*p4.x;
		yt = (1 - t)*(1 - t)*(1 - t)*p1.y + 3 * (1 - t)*(1 - t)*t*p2.y + 3 * (1 - t)*t*t*p3.y + t * t*t*p4.y;
		SDL_RenderDrawPoint(ren, (int)xt, (int)yt);
	}
}


