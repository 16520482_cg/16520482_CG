#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
    RECT rect;
    rect.Left = l;
    rect.Right = r;
    rect.Top = t;
    rect.Bottom = b;

    return rect;
}

CODE Encode(RECT r, Vector2D P)
{
    CODE c = 0;
    if (P.x < r.Left)
        c = c|LEFT;
    if (P.x > r.Right)
        c = c|RIGHT;
    if (P.y < r.Top)
        c = c|TOP;
    if (P.y > r.Bottom)
        c = c|BOTTOM;
    return c;
}

int CheckCase(int c1, int c2)
{
    if (c1 == 0 && c2 == 0)
        return 1;
   if ((c1 != 0) && (c2 != 0) && ((c1&c2) != 0))
        return 2;
    return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	CODE c1, c2;
	c1 = Encode(r, P1);
	c2 = Encode(r, P2);
	int th = CheckCase(c1, c2);
	while (th == 3)
	{
		ClippingCohenSutherland(r, P1, P2);
		c1 = Encode(r, P1);
		c2 = Encode(r, P2);
		th = CheckCase(c1, c2);
	}
	if (th == 2) return 0;
	Q1 = P1;
	Q2 = P2;
	return 1;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	CODE c1, c2;
	c1 = Encode(r, P1);
	c2 = Encode(r, P2);
	float m;
	if (P1.x != P2.x) m = float(P2.y - P1.y) / (P2.x - P1.x);
	if (c1&LEFT != 0) {
		P1.y += m * (r.Left - P1.x);
		P1.x = r.Left;
		return;
	}
	if (c1&RIGHT != 0) {
		P1.y += m * (r.Right - P1.x);
		P1.x = r.Right;
		return;
	}
	if (c1&TOP != 0) {
		if (P1.x != P2.x) P1.x += (r.Top - P1.y) / m;
		P1.y = r.Top;
		return;
	}
	if (c1&BOTTOM != 0) {
		if (P1.x != P2.x) P1.x += (r.Bottom - P1.y) / m;
		P1.y = r.Bottom;
		return;
	}

	if (c2&LEFT != 0) {
		P2.y += m * (r.Left - P2.x);
		P2.x = r.Left;
		return;
	}
	if (c2&RIGHT != 0) {
		P2.y += m * (r.Right - P2.x);
		P2.x = r.Right;
		return;
	}
	if (c2&TOP != 0) {
		if (P1.x != P2.x) P2.x += (r.Top - P2.y) / m;
		P2.y = r.Top;
		return;
	}
	if (c2&BOTTOM != 0) {
		if (P1.x != P2.x) P2.x += (r.Bottom - P2.y) / m;
		P2.y = r.Bottom;
		return;
	}
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
    if (p == 0)
    {
        if (q < 0)
            return 0;
        return 1;
    }

    if (p > 0)
    {
        float t=(float)q/p;
        if(t2<t)
            return 1;
        if(t<t1)
            return 0;
        t2 = t;
        return 1;
    }

    float t=(float)q/p;
    if(t2<t)
        return 0;
    if(t<t1)
        return 1;
    t1 = t;
    return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	float t1 = 0, t2 = 1;
	int dx, dy;
	dx = P2.x - P1.x;
	dy = P2.y - P1.y;
	if (SolveNonLinearEquation(-dx, P1.x - r.Left, t1, t2))
	{
		if (SolveNonLinearEquation(dx, r.Right - P1.x, t1, t2))
		{
			if (SolveNonLinearEquation(-dy, P1.y - r.Top, t1, t2))
			{
				if (SolveNonLinearEquation(dy, r.Bottom - P1.y, t1, t2))
				{
					Q1.x = P1.x + t1 * dx;
					Q1.y = P1.y + t1 * dy;
					Q2.x = P1.x + t2 * dx;
					Q2.y = P1.y + t2 * dy;
					return 1;
				}
			}
		}
	}
	return 0;
}
