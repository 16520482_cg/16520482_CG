#include "DrawPolygon.h"
#include <cmath>
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];
	int PI = 3.14;
	float phi = PI * 0.5;
	for (int i = 0; i < 3; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 3;
	}
	Bresenham_Line(x[0], y[0], x[1], y[1], ren);
	Bresenham_Line(x[0], y[0], x[2], y[2], ren);
	Bresenham_Line(x[1], y[1], x[2], y[2], ren);
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int c = sqrt(R*R*0.5);
	Bresenham_Line(xc - c, yc + c, xc + c, yc + c, ren);
	Bresenham_Line(xc - c, yc + c, xc - c, yc - c, ren);
	Bresenham_Line(xc - c, yc - c, xc + c, yc - c, ren);
	Bresenham_Line(xc + c, yc - c, xc + c, yc + c, ren);
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float PI = 3.14;
	float phi = PI * 0.5;
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += PI * 0.4; 
	}
	Bresenham_Line(x[0], y[0], x[1], y[1], ren);
	Bresenham_Line(x[1], y[1], x[2], y[2], ren);
	Bresenham_Line(x[2], y[2], x[3], y[3], ren);
	Bresenham_Line(x[3], y[3], x[4], y[4], ren);
	Bresenham_Line(x[4], y[4], x[0], y[0], ren);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6], y[6];
	int PI = 3.14;
	float phi = 0;
	for (int i = 0; i < 6; i++) {
		x[i] = xc + int(R*cos(phi)+0.5);
		y[i] = yc - int(R*sin(phi)+0.5);
		phi += PI / 3;
	}
	Bresenham_Line(x[0], y[0], x[1], y[1], ren);
	Bresenham_Line(x[1], y[1], x[2], y[2], ren);
	Bresenham_Line(x[2], y[2], x[3], y[3], ren);
	Bresenham_Line(x[3], y[3], x[4], y[4], ren);
	Bresenham_Line(x[4], y[4], x[5], y[5], ren);
	Bresenham_Line(x[5], y[5], x[0], y[0], ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float PI = 3.14;
	float phi = PI * 0.5;
	for (int i = 0; i < 5; i++) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += PI * 0.4; 
	}
	Bresenham_Line(x[0], y[0], x[2], y[2], ren);
	Bresenham_Line(x[1], y[1], x[3], y[3], ren);
	Bresenham_Line(x[2], y[2], x[4], y[4], ren);
	Bresenham_Line(x[3], y[3], x[0], y[0], ren);
	Bresenham_Line(x[4], y[4], x[1], y[1], ren);
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[10], y[10];
	float PI = 3.14;
	float phi = PI * 0.5;
	float r = R * sin(PI*0.1) / sin(PI*0.7) + 0.5;
	for (int i = 0; i < 10; i+=2) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		x[i + 1] = xc + int(r*cos(phi + PI * 0.2) + 0.5); 
		y[i + 1] = yc - int(r*sin(phi + PI * 0.2) + 0.5); 
		phi += PI * 0.4; 
	}
	Bresenham_Line(x[0], y[0], x[1], y[1], ren);
	Bresenham_Line(x[1], y[1], x[2], y[2], ren);
	Bresenham_Line(x[2], y[2], x[3], y[3], ren);
	Bresenham_Line(x[3], y[3], x[4], y[4], ren);
	Bresenham_Line(x[4], y[4], x[5], y[5], ren);
	Bresenham_Line(x[5], y[5], x[6], y[6], ren);
	Bresenham_Line(x[6], y[6], x[7], y[7], ren);
	Bresenham_Line(x[7], y[7], x[8], y[8], ren);
	Bresenham_Line(x[8], y[8], x[9], y[9], ren);
	Bresenham_Line(x[9], y[9], x[0], y[0], ren);
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[16], y[16];
	float PI = 3.14;
	float phi = PI * 0.5; 
	int r = R * sin(PI*0.1) / sin(PI*0.7);
	for (int i = 0; i < 16; i += 2) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		x[i + 1] = xc + int(r*cos(phi + PI * 0.125) + 0.5); 
		y[i + 1] = yc - int(r*sin(phi + PI * 0.125) + 0.5);
		phi += PI * 0.25; 
	}
	Bresenham_Line(x[0], y[0], x[1], y[1], ren);
	Bresenham_Line(x[1], y[1], x[2], y[2], ren);
	Bresenham_Line(x[2], y[2], x[3], y[3], ren);
	Bresenham_Line(x[3], y[3], x[4], y[4], ren);
	Bresenham_Line(x[4], y[4], x[5], y[5], ren);
	Bresenham_Line(x[5], y[5], x[6], y[6], ren);
	Bresenham_Line(x[6], y[6], x[7], y[7], ren);
	Bresenham_Line(x[7], y[7], x[8], y[8], ren);
	Bresenham_Line(x[8], y[8], x[9], y[9], ren);
	Bresenham_Line(x[9], y[9], x[10], y[10], ren);
	Bresenham_Line(x[10], y[10], x[11], y[11], ren);
	Bresenham_Line(x[11], y[11], x[12], y[12], ren);
	Bresenham_Line(x[12], y[12], x[13], y[13], ren);
	Bresenham_Line(x[13], y[13], x[14], y[14], ren);
	Bresenham_Line(x[14], y[14], x[15], y[15], ren);
	Bresenham_Line(x[15], y[15], x[0], y[0], ren);
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[10], y[10];
	float PI = 3.14;
	float phi = startAngle;
	float r = R * sin(PI*0.1) / sin(PI*0.7);
	for (int i = 0; i < 10; i += 2) {
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		x[i + 1] = xc + int(r*cos(phi + PI * 0.2) + 0.5);
		y[i + 1] = yc - int(r*sin(phi + PI * 0.2) + 0.5);
		phi += PI * 0.4;
	}
	Bresenham_Line(x[0], y[0], x[1], y[1], ren);
	Bresenham_Line(x[1], y[1], x[2], y[2], ren);
	Bresenham_Line(x[2], y[2], x[3], y[3], ren);
	Bresenham_Line(x[3], y[3], x[4], y[4], ren);
	Bresenham_Line(x[4], y[4], x[5], y[5], ren);
	Bresenham_Line(x[5], y[5], x[6], y[6], ren);
	Bresenham_Line(x[6], y[6], x[7], y[7], ren);
	Bresenham_Line(x[7], y[7], x[8], y[8], ren);
	Bresenham_Line(x[8], y[8], x[9], y[9], ren);
	Bresenham_Line(x[9], y[9], x[0], y[0], ren);
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	float PI = 3.14;
	float phi = PI * 0.5;
	while (r > 0) {
		DrawStarAngle(xc, yc, r, phi, ren);
		r *= sin(PI*0.1) / sin(PI*0.7); 
		phi += PI;
	}
}
