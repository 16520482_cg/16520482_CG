#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int a2, b2, x, y, p;
	a2 = a * a;
	b2 = b * b;
    // Area 1
	x = 0; 
	y = b;
	p = -2 * a2 * b + a2 + 2 * b2;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a2 + b2) <= a2 * a2) 
	{
		if (p <= 0) 
		{
			p += 4 * b2 * x + 6 * b2;
		}
		else 
		{
			p += 4 * b2 * x - 4 * a2 * y + 4 * a2 + 6 * b2;
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
    // Area 2
	y = 0;
	x = a;
	p = -2 * b2 * a + b2 + 2 * a2;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a2 + b2) >= a2 * a2)
	{
		if (p <= 0)
		{
			p += 4 * a2 * y + 6 * a2;
		}
		else
		{
			p += 4 * a2 * y - 4 * b2 * x + 4 * b2 + 6 * a2;
			x--;
		}
		y++;
		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int a2, b2, x, y, p;
	a2 = a * a;
	b2 = b * b;
	x = 0;
	y = b;
	p = b2 - a2 * b + 0.25*a2;
    // Area 1
	Draw4Points(xc, yc, x, y, ren);
	while (a2 * y - a2 * 0.5 > b2 * x + b2)
	{
		if (p < 0) 
		{
			p += 2 * b2 * x + 3 * b2;
		}
		else
		{
			p += 2 * b2 * x + 3 * b2 + 2 * a2 - 2 * a2 * y;
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
    // Area 2
	p = b2 * (x + 0.5)*(x + 0.5) + a2 * (y - 1)*(y - 1) - a2 * b2;
	Draw4Points(xc, yc, x, y, ren);
	while (y > 0) 
	{
		if (p < 0)
		{
			p += 2 * b2 * x + 2 * b2 - (2 * a2 * y) + 3 * a2;
			x++; 
		}
		else
		{
			p += -(2 * a2 * y) + 3 * a2;
		}
		y--;
		Draw4Points(xc, yc, x, y, ren);
	}
}